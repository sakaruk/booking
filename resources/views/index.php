<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Rooms management</title>
        <link href="jquery-ui/css/smoothness/jquery-ui.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="jqueryui-editable/css/jqueryui-editable.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/app.css" media="all" rel="stylesheet" type="text/css" />
        <script src="js/jquery.js"></script>
        <script src="jquery-ui/js/jquery-ui.min.js"></script>
        <script src="jqueryui-editable/js/jqueryui-editable.js"></script>
        <script src="js/vue.js"></script>
        <script src="js/main.js"></script>
    </head>

    <body>
        <section class="container bulk-action">
            <form id="bulk-action-form">
                <div class="content-heading">
                    <a href="#" id="bulk-action-link">Bulk Operations</a>
                </div>
                <div class="bulk-action-wrap hide">
                    <div class="form-group odd">
                        <label>Select rooms</label>
                        <select class="rooms-selection">
                            <option id="1">Single Room</option>
                            <option id="2">Double Room</option>
                        </select>
                    </div>
                    <div class="form-group even">
                        <label>Select days</label>
                        <div class="from-to-wrap inline-block">
                            <div class="from">
                                <label>From:</label>
                                <input type="text" class="datepicker">
                            </div>
                            <div class="to">
                                <label>To:</label>
                                <input type="text" class="datepicker">
                            </div>
                        </div>
                        <div class="refine-wrap inline-block">
                            <div class="days-col">
                                Refine days:
                            </div>
                            <div class="days-col">
                                <label><input type="checkbox" value="all"> All Days</label>
                                <label><input type="checkbox" value="weekdays"> All Weekdays</label>
                                <label><input type="checkbox" value="weekends"> All Weekends</label>
                            </div>
                            <div class="days-col">
                                <label><input type="checkbox" value="1"> Mondays</label>
                                <label><input type="checkbox" value="2"> Tuesdays</label>
                                <label><input type="checkbox" value="3"> Wednesdays</label>
                            </div>
                            <div class="days-col">
                                <label><input type="checkbox" value="4"> Thursdays</label>
                                <label><input type="checkbox" value="5"> Fridays</label>
                                <label><input type="checkbox" value="6"> Saturdays</label>
                            </div>
                            <div class="days-col">
                                <label><input type="checkbox" value="0"> Sundays</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group odd">
                        <div class="change-price">
                            <label>Change Price To:</label>
                            <input type="text">
                        </div>
                        <div class="change-availability">
                            <label>Change Availability To:</label>
                            <input type="text">
                        </div>
                    </div>
                    <div class="form-group even">
                        <button class="btn-cancel">Cancel</button>
                        <button class="btn-update">Update</button>
                    </div>
                </div>
            </form>
        </section>
        <section class="container">
            <div class="table-wrap">
                <table class="price-table" border="1" cellpadding="5px">
                    <tbody id="availability-dates">
                        <tr>
                            <td rowspan="3">
                                Price and Availibility
                            </td>
                            <td colspan="31" style="text-align: center;">
                                January 2017
                            </td>
                        </tr>
                        <tr>
                        <?php
                        $start = strtotime('01-01-2017');
                        $end = strtotime('31-01-2017');
                        for($i = $start; $i <= $end; $i = strtotime('+1 day', $i))
                        {
                            $day = date('l', $i);
                            $dayClass = ($day == 'Saturday' || $day == 'Sunday') ? 'weekend' : 'weekday';
                            echo "<td class='$dayClass'>$day</td>";
                        }
                        ?>
                        </tr>
                        <tr>
                        <?php
                        for($i = $start; $i <= $end; $i = strtotime('+1 day', $i))
                        {
                            $date = date('d', $i);
                            echo "<td>$date</td>";
                        }
                        ?>
                        </tr>
                    </tbody>
                    <tbody id="single-room">
                        <tr>
                            <td colspan="32" style="background-color: #c5c690">Single room</td>
                        </tr>
                        <tr>
                            <td>Rooms Available</td>
                            <?php
                            for($i = $start; $i <= $end; $i = strtotime('+1 day', $i))
                            {
                                $date = date('Y-m-d');
                                echo "<td><a href='#' class='number' data-name='inventory'  data-room_id='1' data-date='$date' data-pk='0'>2</a></td>";
                            }
                            ?>
                        </tr>
                        <tr>
                            <td>Price</td>
                            <?php
                            for($i = $start; $i <= $end; $i = strtotime('+1 day', $i))
                            {
                                $date = date('Y-m-d');
                                echo "<td><a href='#' class='price' data-name='price' data-room_id='1' data-date='$date' data-pk='0'>600000</a> IDR</td>";
                            }
                            ?>
                        </tr>
                    </tbody>
                    <tbody id="double-room">
                        <tr>
                            <td colspan="32" style="background-color: #d2e6ee">Double room</td>
                        </tr>
                        <tr>
                            <td>Rooms Available</td>
                            <?php
                            for($i = $start; $i <= $end; $i = strtotime('+1 day', $i))
                            {
                                $date = date('Y-m-d');
                                echo "<td><a href='#' class='number' data-name='inventory' data-room_id='2' data-date='$date' data-pk='0'>2</a></td>";
                            }
                            ?>
                        </tr>
                        <tr>
                            <td>Price</td>
                            <?php
                            for($i = $start; $i <= $end; $i = strtotime('+1 day', $i))
                            {
                                $date = date('Y-m-d');
                                echo "<td><a href='#' class='price' data-name='price' data-room_id='2' data-date='$date' data-pk='0'>600000</a> IDR</td>";
                            }
                            ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
    </body>
</html>