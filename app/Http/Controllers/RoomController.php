<?php namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Room;
use App\RoomDetailByDate;

class RoomController extends BaseController
{
    /**
     * Store shop detail.
     *
     * @return Response
     */
    public function store_room_detail(Request $request)
    {
        $data = $request->all();
        return response()->json(RoomDetailByDate::updateOrCreate(['room_id'=>$data['room_id'],'date'=>$data['date']],$data));
    }
}