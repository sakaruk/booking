<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'name',
        'inventory',
        'color'
    ];
    public $timestamps = false;

    public function room_detail_by_dates()
    {
        return $this->hasMany('App\RoomDetailByDate');
    }
}