<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomDetailByDate extends Model
{
    protected $fillable = [
        'room_id',
        'date',
        'inventory',
        'price'
    ];

    public $timestamps = false;
}