<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('inventory');
            $table->string('color',7);
        });
        DB::table('rooms')->insert([
            [
                'name' => 'Single Room',
                'inventory' => 5,
                'color' => '#c5c690'
            ],
            [
                'name' => 'Double Room',
                'inventory' => 5,
                'color' => '#d2e6ee'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rooms');
    }
}
