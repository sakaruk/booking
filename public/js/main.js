jQuery(document).ready(function(){
    jQuery('#bulk-action-link').on('click',function(event){
        event.preventDefault();
        jQuery('.bulk-action-wrap').toggleClass('hide');
    });
    jQuery('.datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd'
    });
    jQuery('.number, .price').editable({
        type : 'text',
        url: 'api/room_detail',
        ajaxOptions: {
            dataType: 'json'
        },
        params: function(params) {
            var data = {};
            data['id'] = params.pk;
            data['room_id'] = jQuery(this).data('room_id');
            data['date'] = jQuery(this).data('date');
            data[params.name] = params.value;
            return data;
        },
        success: function(response, newValue) {
            if(typeof response.id != 'undefined' ){
                jQuery(this).editable('option', 'pk', response.id);
            }
        }
    });
});