## Booking app
This is a simple rooms management system built using lumen and vue.js

## Installation

### Step 1: Clone the repo
```
git clone https://sakaruk@bitbucket.org/sakaruk/booking.git
```

### Step 2: Setup the database and environment
Copy the file .env.example and make a file .env. Change the database and other necessary details in .env file

### Step 3: Prerequisites
This assumes you know about composer. This will install the dependencies of this app.
```
composer install
php artisan migrate
php -S localhost:8000 -t public
```

